<?php

namespace Zf2tb;

use Zend\EventManager\EventInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;

/**
 * Module
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class Module implements
        AutoloaderProviderInterface,
        ConfigProviderInterface,
        BootstrapListenerInterface,
        ServiceProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * OnBootstrap listener
     * @param $e
     */
    public function onBootstrap(EventInterface $e)
    {
        $application                = $e->getApplication();
        $sm                         = $application->getServiceManager();
        $viewHelperPluginManager    = $sm->get('view_helper_manager');
        /* @var $viewHelperPluginManager \Zend\View\HelperPluginManager */

        //Register Zf2tb view helpers
        $viewHelperConfigurator     = $sm->get('zf2tb_view_helper_configurator');
        /* @var $viewHelperConfigurator \Zf2tb\Form\View\HelperConfig */
        $viewHelperConfigurator->configureServiceManager($viewHelperPluginManager);

        //Register Zf2tb view navigation helpers
        $navViewHelperConfigurator  = $sm->get('zf2tb_nav_view_helper_configurator');
        /* @var $navViewHelperConfigurator \Zf2tb\View\Helper\Navigation\PluginConfigurator */
        $navHelperPluginManager     = $viewHelperPluginManager->get('navigation')->getPluginManager();
        $navViewHelperConfigurator->configureServiceManager($navHelperPluginManager);

        //Prepare the \Zend\Navigation\Page\Mvc for use
        //The pages in navigation container created with a factory have the router injected,
        //but any other explicitly created page needs the router too, so it makes sense to set the default router
        $router                     = $sm->get('router');
        \Zend\Navigation\Page\Mvc::setDefaultRouter($router);
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'zf2tb_view_helper_configurator'  => function(ServiceManager $sm) {
                    $genUtil    = $sm->get('zf2tb_gen_util');
                    $formUtil   = $sm->get('zf2tb_form_util');
                    $instance   = new \Zf2tb\Form\View\HelperConfig($genUtil, $formUtil);
                    return $instance;
                },
            ),
        );
    }
}
