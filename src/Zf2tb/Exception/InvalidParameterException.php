<?php

namespace Zf2tb\Exception;

/**
 * InvalidParameterException
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class InvalidParameterException extends \InvalidArgumentException implements ExceptionInterface
{
}
