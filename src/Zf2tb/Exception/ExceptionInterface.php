<?php

namespace Zf2tb\Exception;

/**
 * ExceptionInterface
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
interface ExceptionInterface
{
}
