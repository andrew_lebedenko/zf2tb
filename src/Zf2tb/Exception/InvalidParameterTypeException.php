<?php

namespace Zf2tb\Exception;

/**
 * InvalidParameterTypeException
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class InvalidParameterTypeException extends \InvalidArgumentException implements ExceptionInterface
{
}
