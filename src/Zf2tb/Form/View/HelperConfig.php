<?php

namespace Zf2tb\Form\View;

use Zf2tb\GenUtil;
use Zf2tb\Form\FormUtil;
use Zf2tb\Form\View\Helper;
use Zend\ServiceManager\ConfigInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * HelperConfig
 * Service manager configuration for form view helpers
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class HelperConfig implements ConfigInterface
{
    /**
     * @var array Pre-aliased view helpers
     */
    protected $invokables = array(
        'formcontrolgrouptb' => 'Zf2tb\Form\View\Helper\FormControlGroupTb',
        'formcontrolstb' => 'Zf2tb\Form\View\Helper\FormControlsTb',
        'formdescriptiontb' => 'Zf2tb\Form\View\Helper\FormDescriptionTb',
        'formelementtb' => 'Zf2tb\Form\View\Helper\FormElementTb',
        'formhiddentb' => 'Zf2tb\Form\View\Helper\FormHiddenTb',
        'formhinttb' => 'Zf2tb\Form\View\Helper\FormHintTb',
    );

    /**
     * @var GenUtil
     */
    protected $genUtil;

    /**
     * @var FormUtil
     */
    protected $formUtil;

    /**
     * Constructor
     * @param GenUtil $genUtil
     * @param FormUtil $formUtil
     */
    public function __construct(GenUtil $genUtil, FormUtil $formUtil)
    {
        $this->genUtil = $genUtil;
        $this->formUtil = $formUtil;
    }

    /**
     * Configure the provided service manager instance with the configuration
     * in this class.
     *
     * In addition to using each of the internal properties to configure the
     * service manager, also adds an initializer to inject ServiceManagerAware
     * classes with the service manager.
     *
     * @param  ServiceManager $serviceManager
     * @return void
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {
        foreach ($this->invokables as $name => $service) {
            $serviceManager->setInvokableClass($name, $service);
        }
        $factories = $this->getFactories();
        foreach ($factories as $name => $factory) {
            $serviceManager->setFactory($name, $factory);
        }
    }

    /**
     * Returns an array of view helper factories
     * @return array
     */
    protected function getFactories()
    {
        $genUtil = $this->genUtil;
        $formUtil = $this->formUtil;
        return array(
            'formactionstb' => function($sm) use ($formUtil) {
                $instance = new Helper\FormActionsTb($formUtil);
                return $instance;
            },
            'formbuttontb' => function($sm) use ($genUtil) {
                $instance = new Helper\FormButtonTb($genUtil);
                return $instance;
            },
            'formcheckboxtb' => function($sm) use ($formUtil) {
                $instance = new Helper\FormCheckboxTb($formUtil);
                return $instance;
            },
            'formelementerrorstb' => function($sm) use ($genUtil) {
                $instance = new Helper\FormElementErrorsTb($genUtil);
                return $instance;
            },
            'formfieldsettb' => function($sm) use ($genUtil, $formUtil) {
                $instance = new Helper\FormFieldsetTb($genUtil, $formUtil);
                return $instance;
            },
            'formfiletb' => function($sm) use ($formUtil) {
                $instance = new Helper\FormFileTb($formUtil);
                return $instance;
            },
            'forminputtb' => function($sm) use ($formUtil) {
                $instance = new Helper\FormInputTb($formUtil);
                return $instance;
            },
            'formlabeltb' => function($sm) use ($genUtil) {
                $formLabelHelper = $sm->get('formLabel');
                $instance = new Helper\FormLabelTb($formLabelHelper, $genUtil);
                return $instance;
            },
            'formmulticheckboxtb' => function($sm) use ($genUtil) {
                $formMultiCheckboxHelper = $sm->get('formMultiCheckbox');
                $instance = new Helper\FormMultiCheckboxTb($formMultiCheckboxHelper, $genUtil);
                return $instance;
            },
            'formpasswordtb' => function($sm) use ($genUtil, $formUtil) {
                $instance = new Helper\FormPasswordTb($genUtil, $formUtil);
                return $instance;
            },
            'formradiotb' => function($sm) use ($genUtil) {
                $formRadioHelper = $sm->get('formRadio');
                $instance = new Helper\FormRadioTb($formRadioHelper, $genUtil);
                return $instance;
            },
            'formresettb' => function($sm) use ($genUtil) {
                $instance = new Helper\FormResetTb($genUtil);
                return $instance;
            },
            'formrowtb' => function($sm) use ($genUtil, $formUtil) {
                $instance = new Helper\FormRowTb($genUtil, $formUtil);
                return $instance;
            },
            'formselecttb' => function($sm) use ($genUtil, $formUtil) {
                $instance = new Helper\FormSelectTb($genUtil, $formUtil);
                return $instance;
            },
            'formsubmittb' => function($sm) use ($genUtil) {
                $instance = new Helper\FormSubmitTb($genUtil);
                return $instance;
            },
            'formtexttb' => function($sm) use ($genUtil, $formUtil) {
                $instance = new Helper\FormTextTb($genUtil, $formUtil);
                return $instance;
            },
            'formtextareatb' => function($sm) use ($genUtil, $formUtil) {
                $instance = new Helper\FormTextareaTb($genUtil, $formUtil);
                return $instance;
            },
            'formtb' => function($sm) use ($genUtil, $formUtil) {
                $instance = new Helper\FormTb($genUtil, $formUtil);
                return $instance;
            },
        );
    }
}
