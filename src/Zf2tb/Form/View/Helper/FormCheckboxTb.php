<?php

namespace Zf2tb\Form\View\Helper;

use Zf2tb\Form\FormUtil;
use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\FormCheckbox;

/**
 * FormCheckboxTb
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class FormCheckboxTb extends FormCheckbox
{
    /**
     * @var FormUtil
     */
    protected $formUtil;

    /**
     * Constructor
     * @param FormUtil $formUtil
     */
    public function __construct(FormUtil $formUtil)
    {
        $this->formUtil  = $formUtil;
    }

    /**
     * Prepares the element prior to rendering
     * @param \Zend\Form\ElementInterface $element
     * @param string $formType
     * @param array $displayOptions
     * @return void
     */
    protected function prepareElementBeforeRendering(ElementInterface $element, $formType, array $displayOptions)
    {
        $this->formUtil->addIdAttributeIfMissing($element);
    }

    /**
     * Render a form <input> checkbox element from the provided $element,
     * @param  ElementInterface $element
     * @param  null|string $formType
     * @param  array $displayOptions
     * @return string
     */
    public function render(ElementInterface $element,
                           $formType = null,
                           array $displayOptions = array()
    ) {
        $this->prepareElementBeforeRendering($element, $formType, $displayOptions);
        $html   = parent::render($element);
        if (null !== ($checkboxLabel = $element->getOption('checkboxLabel'))) {
            if (null !== ($translator = $this->getTranslator())) {
                $checkboxLabel = $translator->translate($checkboxLabel, $this->getTranslatorTextDomain());
            }
            $checkboxLabel = ' ' . $checkboxLabel;
        }
        //Wrap the simple checkbox into label for proper rendering
        $html   = '<label class="checkbox">' . $html . $checkboxLabel . '</label>';
        return $html;
    }

    /**
     * Invoke helper as function
     * Proxies to {@link render()}.
     * @param  ElementInterface|null $element
     * @param  null|string $formType
     * @param  array $displayOptions
     * @return string|FormCheckboxTb
     */
    public function __invoke(ElementInterface $element = null, $formType = null, array $displayOptions = array()
    ) {
        if (!$element) {
            return $this;
        }
        return $this->render($element, $formType, $displayOptions);
    }
}
