<?php

namespace Zf2tb\Form\View\Helper;

use Zend\Form\View\Helper\AbstractHelper as AbstractFormViewHelper;
use Zend\Form\ElementInterface;

/**
 * FormHintTb
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class FormHintTb extends AbstractFormViewHelper
{
    /**
     * Which element types support the inline help?
     * @var array
     */
    protected $supportedTypes = array(
        'text',
        'password',
        'textarea',
        'select',
        'file',
    );

    /**
     * Render element hint from the provided $element
     * @param  ElementInterface $element
     * @return string
     */
    public function render(ElementInterface $element)
    {
        $type = $element->getAttribute('type') || 'text';
        if(!in_array($type, $this->supportedTypes)) {
            return '';
        }
        $escapeHelper = $this->getEscapeHtmlHelper();
        $html = '';
        //Hint
        if(null !== ($hint = $element->getOption('hint'))) {
            $id = 'hint-' . $element->getName();
            if (null !== ($translator = $this->getTranslator())) {
                $hint = $translator->translate($hint, $this->getTranslatorTextDomain());
            }
            $html = sprintf('<span class="help-inline" id="%s">', $id)
                    . $escapeHelper($hint)
                    . '</span>';
        }
        return $html;
    }

    /**
     * Invoke helper as function
     * Proxies to {@link render()}.
     * @param  ElementInterface $element
     * @return string
     */
    public function __invoke(ElementInterface $element)
    {
        return $this->render($element);
    }
}
