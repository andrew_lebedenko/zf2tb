<?php

namespace Zf2tb\Form\View\Helper;

use Zend\Form\View\Helper\AbstractHelper as AbstractViewHelper;
use Zend\Form\ElementInterface;

/**
 * FormControlGroupTb
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class FormControlGroupTb extends AbstractViewHelper
{
    /**
     * Renders the control group div tag
     * @param  ElementInterface $element
     * @param string $content
     * @return string
     */
    public function render(ElementInterface $element, $content)
    {
        $html   = $this->openTag($element);
        $html   .= "\n" . $content;
        $html   .= "\n" . $this->closeTag();
        return $html;
    }

    /**
     * Returns the control group open tag
     * @param ElementInterface $element
     * @return string
     */
    public function openTag(ElementInterface $element)
    {
        $class  = 'control-group';
        $id     = 'cgroup-' . $element->getName();
        if($element->getMessages()) {
            $class  .= ' error';
        }
        $html   = sprintf('<div class="%s" id="%s">', $class, $id);
        return $html;
    }

    /**
     * Returns the control group closing tag
     * @return string
     */
    public function closeTag()
    {
        return '</div>';
    }

    /**
     * Invoke helper as function
     * Proxies to {@link render()}.
     * @param  ElementInterface $element
     * @param string $content
     * @return string
     */
    public function __invoke(ElementInterface $element = null, $content = null)
    {
        if(is_null($element)) {
            return $this;
        } else {
            return $this->render($element, $content);
        }
    }
}
