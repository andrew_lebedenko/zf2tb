<?php

namespace Zf2tb\Form\View\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\AbstractHelper as AbstractFormViewHelper;

/**
 * FormDescriptionTb
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class FormDescriptionTb extends AbstractFormViewHelper
{
    /**
     * Which element types support the description?
     * @var array
     */
    protected $supportedTypes = array(
        'text',
        'password',
        'textarea',
        'checkbox',
        'radio',
        'select',
        'file',
    );

    /**
     * Render a description from the provided $element
     * @param  ElementInterface $element
     * @return string
     */
    public function render(ElementInterface $element)
    {
        $type = $element->getAttribute('type') || 'text';
        if(!in_array($type, $this->supportedTypes)) {
            return '';
        }
        $escapeHelper = $this->getEscapeHtmlHelper();
        $html = '';
        //Description
        if(null !== ($description = $element->getOption('description'))) {
            $id = 'help-' . $element->getName();
            if (null !== ($translator = $this->getTranslator())) {
                $description = $translator->translate($description, $this->getTranslatorTextDomain());
            }
            $html = sprintf('<p class="help-block" id="%s">', $id)
                    . $escapeHelper($description)
                    . '</p>';
        }
        return $html;
    }

    /**
     * Invoke helper as function
     * Proxies to {@link render()}.
     * @param  ElementInterface $element
     * @return string
     */
    public function __invoke(ElementInterface $element)
    {
        return $this->render($element);
    }
}
