<?php

namespace Zf2tb\Form\View\Helper;

use Zf2tb\Form\View\Helper\FormElementTb;
use Zf2tb\Form\View\Helper\FormHintTb;
use Zf2tb\Form\View\Helper\FormDescriptionTb;
use Zf2tb\Form\View\Helper\FormElementErrorsTb;
use Zf2tb\Form\View\Helper\FormControlGroupTb;
use Zf2tb\Form\View\Helper\FormControlsTb;
use Zf2tb\Form\Exception\UnsupportedHelperTypeException;
use Zf2tb\GenUtil;
use Zf2tb\Form\FormUtil;
use Zend\Form\View\Helper\FormLabel;
use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\AbstractHelper;

/**
 * FormRowTb
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class FormRowTb extends AbstractHelper
{
    /**
     * @var FormLabel
     */
    protected $labelHelper;

    /**
     * @var FormElementTb
     */
    protected $elementHelper;

    /**
     * @var FormElementErrorsTb
     */
    protected $elementErrorsHelper;

    /**
     * @var FormHintTb
     */
    protected $hintHelper;

    /**
     * @var FormDescriptionTb
     */
    protected $descriptionHelper;

    /**
     * @var FormControlGroupTb
     */
    protected $controlGroupHelper;

    /**
     * @var FormControlsTb
     */
    protected $controlsHelper;

    /**
     * @var GenUtil
     */
    protected $genUtil;

    /**
     * @var FormUtil
     */
    protected $formUtil;

    /**
     * Constructor
     * @param GenUtil $genUtil
     * @param FormUtil $formUtil
     */
    public function __construct(GenUtil $genUtil, FormUtil $formUtil)
    {
        $this->genUtil  = $genUtil;
        $this->formUtil = $formUtil;
    }

    /**
     * Utility form helper that renders a label (if it exists), an element, hint, description and errors
     * @param ElementInterface $element
     * @param string|null $formType
     * @param array $displayOptions
     * @param bool $renderErrors
     * @return string
     */
    public function render(ElementInterface $element,
                           $formType = null,
                           array $displayOptions = array(),
                           $renderErrors = true)
    {
        $formType = $this->formUtil->filterFormType($formType);

        $elementHelper = $this->getElementHelper();
        $elementErrorsHelper = $this->getElementErrorsHelper();
        $hintHelper = $this->getHintHelper();
        $descriptionHelper = $this->getDescriptionHelper();

        $label = (string)$element->getLabel();
        $elementString = $elementHelper->render($element, $formType, $displayOptions);

        //Hint, description and element errors are generated only for visible elements on horizontal and vertical forms
        //Divs for control-group and controls are generated only for visible elements on horizontal and vertical forms,
        //otherwise a blank vertical space is rendered
        if (($formType == FormUtil::FORM_TYPE_HORIZONTAL || $formType == FormUtil::FORM_TYPE_VERTICAL)
            && !($element instanceof \Zend\Form\Element\Hidden)
            && !($element instanceof \Zend\Form\Element\Csrf)) {

            $controlGroupHelper = $this->getControlGroupHelper();
            $controlGroupOpen = $controlGroupHelper->openTag($element);
            $controlGroupClose = $controlGroupHelper->closeTag();
            $controlsHelper = $this->getControlsHelper();
            $controlsOpen = $controlsHelper->openTag($element);
            $controlsClose = $controlsHelper->closeTag();
            $hint = $hintHelper->render($element);
            $description = $descriptionHelper->render($element);
            if ($renderErrors) {
                $elementErrors = $elementErrorsHelper->render($element);
            } else {
                $elementErrors = '';
            }
        } else {
            $controlGroupOpen = '';
            $controlGroupClose = '';
            //We need some whitespace between label and element on inline and search forms
            $controlsOpen = "\n";
            $controlsClose = '';
            $hint = '';
            $description = '';
            $elementErrors = '';
        }

        if (!empty($label)) {
            //Element has a label
            $labelHelper = $this->getLabelHelper();
            $label = $labelHelper($element, $displayOptions);
        }
        $markup = $controlGroupOpen
                . $label
                . $controlsOpen
                . $elementString
                . $hint
                . $description
                . $elementErrors
                . $controlsClose
                . $controlGroupClose;
        
        return $markup;
    }

    /**
     * Invoke helper as a function
     * Proxies to {@link render()}.
     * @param null|ElementInterface $element
     * @param string|null $formType
     * @param array $displayOptions
     * @param bool $renderErrors
     * @return string|FormRowTb
     */
    public function __invoke(ElementInterface $element = null,
                             $formType = null,
                             array $displayOptions = array(),
                             $renderErrors = true) {
        if (!$element) {
            return $this;
        }
        return $this->render($element, $formType, $displayOptions, $renderErrors);
    }

    /**
     * Retrieve the FormLabelTb helper
     * @return FormLabelTb
     * @throws \Zf2tb\Form\Exception\UnsupportedHelperTypeException
     */
    protected function getLabelHelper()
    {
        if (!$this->labelHelper) {
            if (method_exists($this->view, 'plugin')) {
                $this->labelHelper = $this->view->plugin('form_label_tb');
            }
            if (!$this->labelHelper instanceof FormLabelTb) {
                throw new UnsupportedHelperTypeException('Label helper (FormLabelTb) unavailable or unsupported type.');
            }
        }
        return $this->labelHelper;
    }

    /**
     * Retrieve the FormElementTb helper
     * @return FormElementTb
     * @throws \Zf2tb\Form\Exception\UnsupportedHelperTypeException
     */
    protected function getElementHelper()
    {
        if (!$this->elementHelper) {
            if (method_exists($this->view, 'plugin')) {
                $this->elementHelper = $this->view->plugin('form_element_tb');
            }
            if (!$this->elementHelper instanceof FormElementTb) {
                throw new UnsupportedHelperTypeException('Element helper (FormElementTb) unavailable or unsupported type.');
            }
        }
        return $this->elementHelper;
    }

    /**
     * Retrieve the FormElementErrorsTb helper
     * @return FormElementErrorsTb
     * @throws \Zf2tb\Form\Exception\UnsupportedHelperTypeException
     */
    protected function getElementErrorsHelper()
    {
        if (!$this->elementErrorsHelper) {
            if (method_exists($this->view, 'plugin')) {
                $this->elementErrorsHelper = $this->view->plugin('form_element_errors_tb');
            }
            if (!$this->elementErrorsHelper instanceof FormElementErrorsTb) {
                throw new UnsupportedHelperTypeException('Element errors helper (FormElementErrorsTb) unavailable or unsupported type.');
            }
        }
        return $this->elementErrorsHelper;
    }

    /**
     * Retrieve the FormHintTb helper
     * @return FormHintTb
     * @throws \Zf2tb\Form\Exception\UnsupportedHelperTypeException
     */
    protected function getHintHelper()
    {
        if (!$this->hintHelper) {
            if (method_exists($this->view, 'plugin')) {
                $this->hintHelper = $this->view->plugin('form_hint_tb');
            }
            if (!$this->hintHelper instanceof FormHintTb) {
                throw new UnsupportedHelperTypeException('Hint helper (FormHintTb) unavailable or unsupported type.');
            }
        }
        return $this->hintHelper;
    }

    /**
     * Retrieve the FormDescriptionTb helper
     * @return FormDescriptionTb
     * @throws \Zf2tb\Form\Exception\UnsupportedHelperTypeException
     */
    protected function getDescriptionHelper()
    {
        if (!$this->descriptionHelper) {
            if (method_exists($this->view, 'plugin')) {
                $this->descriptionHelper = $this->view->plugin('form_description_tb');
            }
            if (!$this->descriptionHelper instanceof FormDescriptionTb) {
                throw new UnsupportedHelperTypeException('Description helper (FormDescriptionTb) unavailable or unsupported type.');
            }
        }
        return $this->descriptionHelper;
    }

    /**
     * Retrieve the FormControlGroupTb helper
     * @return FormControlGroupTb
     * @throws \Zf2tb\Form\Exception\UnsupportedHelperTypeException
     */
    protected function getControlGroupHelper()
    {
        if (!$this->controlGroupHelper) {
            if (method_exists($this->view, 'plugin')) {
                $this->controlGroupHelper = $this->view->plugin('form_control_group_tb');
            }
            if (!$this->controlGroupHelper instanceof FormControlGroupTb) {
                throw new UnsupportedHelperTypeException('Control group helper (FormControlGroupTb) unavailable or unsupported type.');
            }
        }
        return $this->controlGroupHelper;
    }

    /**
     * Retrieve the FormControlsTb helper
     * @return FormControlsTb
     * @throws \Zf2tb\Form\Exception\UnsupportedHelperTypeException
     */
    protected function getControlsHelper()
    {
        if (!$this->controlsHelper) {
            if (method_exists($this->view, 'plugin')) {
                $this->controlsHelper = $this->view->plugin('form_controls_tb');
            }
            if (!$this->controlsHelper instanceof FormControlsTb) {
                throw new UnsupportedHelperTypeException('Controls helper (FormControlsTb) unavailable or unsupported type.');
            }
        }
        return $this->controlsHelper;
    }
}
