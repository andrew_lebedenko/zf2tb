<?php

namespace Zf2tb\Form\View\Helper;

use \Zf2tb\GenUtil;
use \Zf2tb\Form\FormUtil;
use Zend\Form\View\Helper\FormSelect;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;

/**
 * FormSelectTb
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class FormSelectTb extends FormSelect
{
    /**
     * General utils
     * @var GenUtil
     */
    protected $genUtil;

    /**
     * Form utils
     * @var \Zf2tb\Form\FormUtil
     */
    protected $formUtil;

    /**
     * Constructor
     * @param \Zf2tb\GenUtil $genUtil
     * @param \Zf2tb\Form\FormUtil $formUtil
     */
    public function __construct(GenUtil $genUtil, FormUtil $formUtil)
    {
        $this->genUtil  = $genUtil;
        $this->formUtil = $formUtil;
    }

    /**
     * Render a form <select> element from the provided $element
     * @param  ElementInterface $element
     * @param null|string $formType
     * @param array $displayOptions
     * @return string
     */
    public function render(ElementInterface $element, $formType = null, array $displayOptions = array())
    {
        if (array_key_exists('class', $displayOptions)) {
            $class                  = $element->getAttribute('class');
            $class                  = $this->genUtil->addWords($displayOptions['class'], $class);
            $escapeHtmlAttrHelper   = $this->getEscapeHtmlAttrHelper();
            $class                  = $this->genUtil->escapeWords($class, $escapeHtmlAttrHelper);
            $element->setAttribute('class', $class);
        }
        if (array_key_exists('size', $displayOptions)) {
            $element->setAttribute('size', (int)$displayOptions['size']);
        }
        $this->formUtil->addIdAttributeIfMissing($element);
        $html               = parent::render($element);
        return $html;
    }

    /**
     * Invoke helper as function
     * Proxies to {@link render()}.
     * @param  ElementInterface $element
     * @param null|string $formType
     * @param array $displayOptions
     * @return string|FormSelectTb
     */
    public function __invoke(ElementInterface $element = null, $formType = null, array $displayOptions = array())
    {
        if (!$element) {
            return $this;
        }
        return $this->render($element, $formType, $displayOptions);
    }
}
