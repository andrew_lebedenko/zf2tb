<?php

namespace Zf2tb\Form\View\Helper;

use Zf2tb\GenUtil;
use Zend\Form\View\Helper\AbstractHelper as AbstractFormViewHelper;
use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\FormMultiCheckbox;

/**
 * FormMulticheckboxTb
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class FormMultiCheckboxTb extends AbstractFormViewHelper
{
    /**
     * @var array
     */
    protected $tbLabelAttributes = array(
        'class'     => 'checkbox',
    );

    /**
     * @var GenUtil
     */
    protected $genUtil;

    /**
     * ZF2 multi checkbox helper
     * @var FormMultiCheckbox
     */
    protected $formMultiCheckboxHelper;

    /**
     * Constructor
     * @param \Zend\Form\View\Helper\FormMultiCheckbox $formMultiCheckboxHelper
     * @param \Zf2tb\GenUtil $genUtil
     */
    public function __construct(FormMultiCheckbox $formMultiCheckboxHelper, GenUtil $genUtil)
    {
        $this->formMultiCheckboxHelper  = $formMultiCheckboxHelper;
        $this->genUtil                  = $genUtil;
    }

    /**
     * Invoke helper as function
     * Proxies to {@link render()}.
     * @param  ElementInterface|null $element
     * @param null|string $formType
     * @param array $displayOptions
     * @return string|FormMultiCheckboxTb
     */
    public function __invoke(ElementInterface $element = null, $formType = null, array $displayOptions = array())
    {
        if (!$element) {
            return $this;
        }
        return $this->render($element, $formType, $displayOptions);
    }

    /**
     * Render a form <input> element from the provided $element
     * @param  ElementInterface $element
     * @param null|string $formType
     * @param array $displayOptions
     * @return string
     */
    public function render(ElementInterface $element, $formType = null, array $displayOptions = array())
    {
        $labelAttributes    = $this->tbLabelAttributes;
        if(array_key_exists('inline', $displayOptions) && $displayOptions['inline'] == true) {
            $labelAttributes = $this->genUtil->addWordsToArrayItem('inline', $labelAttributes, 'class');
        }
        $formMultiCheckboxHelper    = $this->formMultiCheckboxHelper;
        $formMultiCheckboxHelper->setLabelAttributes($labelAttributes);
        return $formMultiCheckboxHelper($element);
    }
}
