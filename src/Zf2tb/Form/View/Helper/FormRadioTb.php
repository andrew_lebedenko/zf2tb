<?php

namespace Zf2tb\Form\View\Helper;

use Zf2tb\GenUtil;
use Zend\Form\View\Helper\AbstractHelper as AbstractFormViewHelper;
use Zend\Form\View\Helper\FormRadio;
use Zend\Form\ElementInterface;

/**
 * FormRadioTb
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class FormRadioTb extends AbstractFormViewHelper
{
    /**
     * @var array
     */
    protected $tbLabelAttributes = array(
        'class' => 'radio',
    );

    /**
     * @var GenUtil
     */
    protected $genUtil;

    /**
     * ZF2 radio helper
     * @var FormRadio
     */
    protected $formRadioHelper;

    /**
     * Constructor
     * @param FormRadio $formRadioHelper
     * @param GenUtil $genUtil
     */
    public function __construct(FormRadio $formRadioHelper, GenUtil $genUtil)
    {
        $this->formRadioHelper  = $formRadioHelper;
        $this->genUtil          = $genUtil;
    }

    /**
     * Invoke helper as function
     * Proxies to {@link render()}.
     * @param  ElementInterface|null $element
     * @param null|string $formType
     * @param array $displayOptions
     * @return string|FormRadioTb
     */
    public function __invoke(ElementInterface $element = null, $formType = null, array $displayOptions = array())
    {
        if (!$element) {
            return $this;
        }
        return $this->render($element, $formType, $displayOptions);
    }

    /**
     * Render a form <input> element from the provided $element
     * @param  ElementInterface $element
     * @param null|string $formType
     * @param array $displayOptions
     * @return string
     */
    public function render(ElementInterface $element, $formType = null, array $displayOptions = array())
    {
        $labelAttributes    = $this->tbLabelAttributes;
        if(array_key_exists('inline', $displayOptions) && $displayOptions['inline'] == true) {
            $labelAttributes = $this->genUtil->addWordsToArrayItem('inline', $labelAttributes, 'class');
        }
        $formRadioHelper    = $this->formRadioHelper;
        $formRadioHelper->setLabelAttributes($labelAttributes);
        return $formRadioHelper($element);
    }
}
