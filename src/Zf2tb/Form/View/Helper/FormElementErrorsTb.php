<?php

namespace Zf2tb\Form\View\Helper;

use Zf2tb\GenUtil;
use Zend\Form\View\Helper\FormElementErrors;
use Zend\Form\ElementInterface;

/**
 * FormElementErrorsTb
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class FormElementErrorsTb extends FormElementErrors
{
    /**
     * General utils
     * @var GenUtil
     */
    protected $genUtil;

    /**
     * Constructor
     * @param \Zf2tb\GenUtil $genUtil
     */
    public function __construct(GenUtil $genUtil)
    {
        $this->genUtil  = $genUtil;
    }

    /**
     * Render validation errors for the provided $element
     * @param  ElementInterface $element
     * @param array $attributes
     * @return string
     */
    public function render(ElementInterface $element, array $attributes = array())
    {
        $attributes = $this->genUtil->addWordsToArrayItem('errors', $attributes, 'class');
        return parent::render($element, $attributes);
    }

}
