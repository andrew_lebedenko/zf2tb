<?php

namespace Zf2tb\Form\View\Helper;

use Zf2tb\Form\FormUtil;
use Zf2tb\GenUtil;
use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\FormText;

/**
 * FormTextTb
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class FormTextTb extends FormText
{
    /**
     * @var FormUtil
     */
    protected $formUtil;

    /**
     * @var GenUtil
     */
    protected $genUtil;

    /**
     * Constructor
     * @param \Zf2tb\GenUtil $genUtil
     * @param \Zf2tb\Form\FormUtil $formUtil
     */
    public function __construct(GenUtil $genUtil, FormUtil $formUtil)
    {
        $this->genUtil = $genUtil;
        $this->formUtil = $formUtil;
    }

    /**
     * Prepares the element prior to rendering
     * @param \Zend\Form\ElementInterface $element
     * @param string $formType
     * @param array $displayOptions
     * @return void
     */
    protected function prepareElementBeforeRendering(ElementInterface $element, $formType, array $displayOptions)
    {
        $class = $element->getAttribute('class');
        if (array_key_exists('class', $displayOptions)) {
            $class = $this->genUtil->addWords($displayOptions['class'], $class);
        }
        if ($formType == FormUtil::FORM_TYPE_SEARCH) {
            $class = $this->genUtil->addWords('search-query', $class);
        }
        $escapeHtmlAttrHelper = $this->getEscapeHtmlAttrHelper();
        $class = $this->genUtil->escapeWords($class, $escapeHtmlAttrHelper);
        $element->setAttribute('class', $class);
        $this->formUtil->addIdAttributeIfMissing($element);
    }

    /**
     * Render a form <input> text element from the provided $element,
     * @param  ElementInterface $element
     * @param  null|string $formType
     * @param  array $displayOptions
     * @return string
     */
    public function render(ElementInterface $element,
                           $formType = null,
                           array $displayOptions = array()
    ) {
        $formType = $this->formUtil->filterFormType($formType);
        $this->prepareElementBeforeRendering($element, $formType, $displayOptions);
        $html = parent::render($element);
        //Text prepend / append
        $escapeHelper = $this->getEscapeHtmlHelper();
        $escapeAttribHelper = $this->getEscapeHtmlAttrHelper();
        $prepAppClass = '';
        //Prepend icon (not possible on Search forms)
        if (($formType != FormUtil::FORM_TYPE_SEARCH) && array_key_exists('prependIcon', $displayOptions)) {
            $prepAppClass = $this->genUtil->addWords('input-prepend', $prepAppClass);
            $html = sprintf('<span class="add-on"><i class="%s"></i></span>%s',
                    $escapeAttribHelper($displayOptions['prependIcon']), $html);
        }
        //Prepend text
        if (null !== ($prependText = $element->getOption('prependText'))) {
            $prepAppClass = $this->genUtil->addWords('input-prepend', $prepAppClass);
            if (null !== ($translator = $this->getTranslator())) {
                $prependText = $translator->translate($prependText, $this->getTranslatorTextDomain());
            }
            $html = '<span class="add-on">'
                . $escapeHelper($prependText) . '</span>'
                . $html;
        }
        //Append icon (not possible on Search forms)
        if (($formType != FormUtil::FORM_TYPE_SEARCH) && array_key_exists('appendIcon', $displayOptions)) {
            $prepAppClass = $this->genUtil->addWords('input-append', $prepAppClass);
            $html .= sprintf('<span class="add-on"><i class="%s"></i></span>',
                    $escapeAttribHelper($displayOptions['appendIcon']));
        }
        //Append text
        if (null !== ($appendText = $element->getOption('appendText'))) {
            $prepAppClass = $this->genUtil->addWords('input-append', $prepAppClass);
            if (null !== ($translator = $this->getTranslator())) {
                $appendText = $translator->translate($appendText, $this->getTranslatorTextDomain());
            }
            $html .= '<span class="add-on">' . $escapeHelper($appendText) . '</span>';
        }
        if ($prepAppClass) {
            $html = '<div class="' . $prepAppClass . '">' . "\n$html\n" . '</div>';
        }
        return $html;
    }

    /**
     * Invoke helper as function
     * Proxies to {@link render()}.
     * @param  ElementInterface|null $element
     * @param  null|string $formType
     * @param  array $displayOptions
     * @return string|FormTextTb
     */
    public function __invoke(ElementInterface $element = null, $formType = null, array $displayOptions = array()
    ) {
        if (!$element) {
            return $this;
        }
        return $this->render($element, $formType, $displayOptions);
    }
}
