<?php

namespace Zf2tb\Form\Exception;

use Zf2tb\Form\Exception\ExceptionInterface;

/**
 * UnsupportedHelperTypeException
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class UnsupportedHelperTypeException extends \Exception implements ExceptionInterface
{
}
