<?php

namespace Zf2tb\View\Helper\Navigation;

use Zend\Navigation\Page\AbstractPage;

/**
 * Abstract Navigation Helper
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
abstract class AbstractNavHelper extends AbstractHelper
{
    protected function decorateContainer($content, array $options = array()) {
        //Align option
        if(array_key_exists('align', $options)) {
            $align = $options['align'];
        } else {
            $align = null;
        }

        $ulClass = $this->ulClass;

        if($align == self::ALIGN_LEFT) {
            $this->addWord('pull-left', $ulClass);
        } elseif($align == self::ALIGN_RIGHT) {
            $this->addWord('pull-right', $ulClass);
        }

        $html = '<ul class="' . $ulClass . '" role="menu">';
        $html .= PHP_EOL . $content;
        $html .= PHP_EOL . '</ul>';

        return $html;
    }

    protected function decorateNavHeader($content,
                                         AbstractPage $item,
                                         array $options = array()) {
        return $this->decorateNavHeaderInDropdown($content, $item, $options);
    }

    protected function decorateDivider($content,
                                       AbstractPage $item,
                                       array $options = array()) {
        return $this->decorateDividerInDropdown($content, $item, $options);
    }

    protected function decorateLink($content,
                                    AbstractPage $page,
                                    array $options = array()) {
        return $this->decorateLinkInDropdown($content, $page, $options);
    }

    protected function decorateDropdown($content,
                                        AbstractPage $page,
                                        array $options = array()) {
        //Get attribs
        $liAttribs = array(
            'id' => $page->getId(),
            'class' => $this->parentMenuClass . ($page->isActive(true) ? ' active' : ''),
        );
        $html = PHP_EOL . '<li' . $this->htmlAttribs($liAttribs) . '>'
              . PHP_EOL . $content
              . PHP_EOL . '</li>';

        return $html;
    }
}
