<?php

namespace Zf2tb\View\Helper\Navigation;

use Zend\Navigation\AbstractContainer;
use Zend\Navigation\Navigation;
use Zend\Navigation\Page\AbstractPage;
use Zend\View\Helper\AbstractHelper as ZfAbstractHelper;
use Zf2tb\View\Helper\Navigation\Exception\UnsupportedElementTypeException;

/**
 * TbNavbar
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class TbNavbar extends AbstractNavHelper
{
    /**
     * Renders helper
     * @param  string|\Zend\Navigation\AbstractContainer $container [optional] container to render.
     *                                         Default is null, which indicates
     *                                         that the helper should render
     *                                         the container returned by {@link
     *                                         getContainer()}.
     * @return string helper output
     * @throws \Zend\View\Exception\ExceptionInterface if unable to render
     */
    public function render($container = null)
    {
        return $this->renderNavbar($container);
    }

    public function renderNavbar(Navigation $container = null, $leftElements = null, $rightElements = null, AbstractPage $brandLink = null, $brandName = null, $fixed = false, $fixedBottom = false, $responsive = true, $inverse = false, $fluid = true)
    {
        if (null === $container) {
            $container = $this->getContainer();
        }
        if ($leftElements && !is_array($leftElements)) {
            $leftElements = array($leftElements);
        }
        if ($rightElements && !is_array($rightElements)) {
            $rightElements = array($rightElements);
        }
        $html = '';

        //Navbar scaffolding
        $navbarClass = 'navbar';
        if ($fixed) {
            if ($fixedBottom) {
                $navbarClass .= ' navbar-fixed-bottom';
            } else {
                $navbarClass .= ' navbar-fixed-top';
            }
        }
        if ($inverse) {
            $navbarClass .= ' navbar-inverse';
        }
        $html .= '<div class="' . $navbarClass . '">';
        $html .= PHP_EOL . '<div class="navbar-inner">';
        if ($fluid) {
            $html .= PHP_EOL . '<div class="container-fluid">';
        } else {
            $html .= PHP_EOL . '<div class="container">';
        }

        //Responsive (button)
        if ($responsive) {
            $html .= PHP_EOL . '<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">';
            $html .= PHP_EOL . '<span class="icon-bar"></span>';
            $html .= PHP_EOL . '<span class="icon-bar"></span>';
            $html .= PHP_EOL . '<span class="icon-bar"></span>';
            $html .= PHP_EOL . '</a>';
        }

        //Brand
        if ($brandLink) {
            $view = $this->getView();
            if ($brandName) {
                $brandName = $view->escapeHtml($brandName);
            } else {
                $brandName = $view->escapeHtml($brandLink->getLabel());
            }
            $html .= PHP_EOL . '<a class="brand" href="' . $brandLink->getHref() . '">' . $brandName . '</a>';
        }

        //Responsive (div)
        if ($responsive) {
            $html .= PHP_EOL . '<div class="nav-collapse">';
        }

        //Primary container
        $options = array(
            'align' => null,
            'ulClass' => 'nav',
        );
        $html .= PHP_EOL . $this->renderContainer($container, $options);

        //Left elements
        if ($leftElements) {
            $html .= PHP_EOL . $this->renderElements($leftElements, 'left');
        }

        //Right elements
        if ($rightElements) {
            $html .= PHP_EOL . $this->renderElements($rightElements, 'right');
        }

        //Responsive (close div)
        if ($responsive) {
            $html .= PHP_EOL . '</div>';
        }

        //Scaffolding (close divs)
        $html .= PHP_EOL . '</div>';
        $html .= PHP_EOL . '</div>';
        $html .= PHP_EOL . '</div>';

        return $html;
    }

    protected function renderElements(array $elements, $align = null)
    {
        $html = '';
        $view = $this->getView();
        foreach ($elements as $element) {
            if ($element instanceof AbstractContainer) {
                $options = array(
                    'align' => $align,
                    'ulClass' => 'nav',
                );
                $html .= PHP_EOL . $this->renderContainer($element, $options);
            }
            //TODO - implement support for search and inline forms
            /*
              elseif ($element instanceof \Zf2tb\Form\Search) {
              $class  = $element->getAttrib('class');
              $this->addWord('navbar-search', $class);
              if($align == self::ALIGN_LEFT) {
              $this->addWord('pull-left', $class);
              } elseif($align == self::ALIGN_RIGHT) {
              $this->addWord('pull-right', $class);
              }
              $element->setAttrib('class', $class);
              $html   .= PHP_EOL . $element->render();
              } elseif ($element instanceof \Zf2tb\Form\Inline) {
              $class  = $element->getAttrib('class');
              $this->addWord('navbar-form', $class);
              if($align == self::ALIGN_LEFT) {
              $this->addWord('pull-left', $class);
              } elseif($align == self::ALIGN_RIGHT) {
              $this->addWord('pull-right', $class);
              }
              $element->setAttrib('class', $class);
              $html   .= PHP_EOL . $element->render();
              }
             */ elseif ($element instanceof ZfAbstractHelper) {
                $class = $element->getAttrib('class');
                if ($align == self::ALIGN_LEFT) {
                    $this->addWord('pull-left', $class);
                } elseif ($align == self::ALIGN_RIGHT) {
                    $this->addWord('pull-right', $class);
                }
                $element->setAttrib('class', $class);
                $html .= PHP_EOL . $element->render();
            } elseif (is_string($element)) {
                $pClass = 'navbar-text';
                if ($align == self::ALIGN_LEFT) {
                    $pClass .= ' pull-left';
                } elseif ($align == self::ALIGN_RIGHT) {
                    $pClass .= ' pull-right';
                }
                $html .= PHP_EOL . '<p class="' . $pClass . '">' . $view->escapeHtml($element) . '</p>';
            } else {
                throw new UnsupportedElementTypeException('Unsupported element type.');
            }
        }
        return $html;
    }

}
