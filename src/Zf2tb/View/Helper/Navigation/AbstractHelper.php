<?php

namespace Zf2tb\View\Helper\Navigation;

use Zend\Navigation\Navigation;
use Zend\Navigation\Page\AbstractPage;
use Zend\View\Helper\Navigation\AbstractHelper as AbstractZfNavigationHelper;

/**
 * Abstract Helper
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
abstract class AbstractHelper extends AbstractZfNavigationHelper
{
    const ALIGN_LEFT = 'left';

    const ALIGN_RIGHT = 'right';

    /**
     * CSS class to use for the ul element
     *
     * @var string
     */
    protected $ulClass = 'nav';

    protected $inverseIcon = false;

    /**
     * Css class to use for the li element containing dropdown submenu
     *
     * @var string
     */
    protected $parentMenuClass = 'dropdown';

    /**
     * Css class to use for the ul element of dropdown submenu
     *
     * @var string
     */
    protected $dropdownUlClass = 'dropdown-menu';

    /**
     * Whether icons should be rendered
     *
     * @var boolean
     */
    protected $renderIcons = true;

    /**
     * Whether caret should be rendered for dropdown parent item
     *
     * @var boolean
     */
    protected $useDropdownParentCaret = true;

    protected function renderContainer(Navigation $container, array $options = array()) {
        $pages = $container->getPages();
        $html = '';
        foreach ($pages as $page) {
            /* @var $page \Zend\Navigation\Page\AbstractPage */
            if($page->hasPages()) {
                //Dropdown menu
                $html .= PHP_EOL . $this->renderDropdown($page, $options);
            } else {
                $html .= PHP_EOL . $this->renderItem($page, false, $options);
            }
        }
        $html = $this->decorateContainer($html, $options);
        return $html;
    }

    abstract protected function decorateContainer($content, array $options = array());

    protected function renderItem(AbstractPage $page,
                                  $renderInDropdown = false,
                                  array $options = array())
    {
        if(!$this->accept($page)) {
            return '';
        }
        if($page->navHeader) {
            //Nav Header
            if($renderInDropdown) {
                $itemHtml = $this->renderNavHeaderInDropdown($page, $options);
                $html = $this->decorateNavHeaderInDropdown($itemHtml, $page, $options);
            } else {
                $itemHtml = $this->renderNavHeader($page, $options);
                $html = $this->decorateNavHeader($itemHtml, $page, $options);
            }
        } elseif($page->divider) {
            //Divider
            if($renderInDropdown) {
                $itemHtml = $this->renderDividerInDropdown($page, $options);
                $html = $this->decorateDividerInDropdown($itemHtml, $page, $options);
            } else {
                $itemHtml = $this->renderDivider($page, $options);
                $html = $this->decorateDivider($itemHtml, $page, $options);
            }
        } else {
            //Nav link
            if($renderInDropdown) {
                $itemHtml = $this->renderLinkInDropdown($page, $options);
                $html = $this->decorateLinkInDropdown($itemHtml, $page, $options);
            } else {
                $itemHtml = $this->renderLink($page, $options);
                $html = $this->decorateLink($itemHtml, $page, $options);
            }
        }
        return $html;
    }

    protected function renderNavHeader(AbstractPage $item, array $options = array()) {
        $icon = $this->htmlifyIcon($item);
        $label = $this->translate($item->getLabel());
        $html = $icon . $this->getView()->escapeHtml($label);
        return $html;
    }

    protected function renderNavHeaderInDropdown(AbstractPage $item,
                                                 array $options = array()) {
        $html   = $this->renderNavHeader($item, $options);
        return $html;
    }

    abstract protected function decorateNavHeader($content,
                                                  AbstractPage $item,
                                                  array $options = array());

    protected function decorateNavHeaderInDropdown($content,
                                                   AbstractPage $item,
                                                   array $options = array()) {
        $html = '<li class="nav-header">' . $content . '</li>';
        return $html;
    }



    protected function renderDivider(AbstractPage $item, array $options = array()) {
        return '';
    }

    protected function renderDividerInDropdown(AbstractPage $item, array $options = array()) {
        $html = $this->renderDivider($item, $options);
        return $html;
    }

    abstract protected function decorateDivider($content,
                                                AbstractPage $item,
                                                array $options = array());

    protected function decorateDividerInDropdown($content,
                                                 AbstractPage $item,
                                                 array $options = array()) {
        $html = '<li class="divider">' . $content . '</li>';
        return $html;
    }

    protected function renderLink(AbstractPage $page, array $options = array()) {
        //Assemble html
        $html = $this->htmlifyA($page);
        return $html;
    }

    protected function renderLinkInDropdown(AbstractPage $page,
                                            array $options = array()) {
        $html = $this->renderLink($page, $options);
        return $html;
    }

    abstract protected function decorateLink($content,
                                             AbstractPage $page,
                                             array $options = array());

    protected function decorateLinkInDropdown($content,
                                              AbstractPage $page,
                                              array $options = array()) {
        //Active
        if($page->isActive(true)) {
            $liClass = ' class="active"';
        } else {
            $liClass = '';
        }
        $html = '<li' . $liClass . '>' . $content . '</li>';
        return $html;
    }

    protected function renderDropdown(AbstractPage $page, array $options = array()) {
        //Get label and title
        $label = $this->translate($page->getLabel());
        $title = $this->translate($page->getTitle());
        $escaper = $this->view->plugin('escapeHtml');
        //Get attribs
        $class = $page->getClass();
        $this->addWord('dropdown-toggle', $class);
        $aAttribs = array(
            'title' => $title,
            'class' => $class,
            'data-toggle' => 'dropdown',
            'href' => '#',
        );
        if($this->renderIcons) {
            $iconHtml = $this->htmlifyIcon($page);
        } else {
            $iconHtml = '';
        }
        $html = '<a' . $this->htmlAttribs($aAttribs) . '>'
            . $iconHtml . $escaper($label);
        if ($this->useDropdownParentCaret) {
            $html .= ' <b class="caret"></b>';
        }
        $html .= '</a>' . PHP_EOL . '<ul class="' . $this->dropdownUlClass . '">';
        $pages = $page->getPages();
        foreach($pages as $dropdownPage) {
            /* @var $dropdownPage \Zend\Navigation\Page\AbstractPage */
            $html .= PHP_EOL . $this->renderItem($dropdownPage, true, $options);
        }
        $html .= PHP_EOL . '</ul>';
        $html = $this->decorateDropdown($html, $page, $options);
        return $html;
    }

    abstract protected function decorateDropdown($content,
                                                 AbstractPage $page,
                                                 array $options = array());

    /**
     * Returns an HTML string containing an 'a' element for the given page
     * @param \Zend\Navigation\Page\AbstractPage $page
     * @param bool $renderIcons
     * @param bool $activeIconInverse
     * @return string
     */
    public function htmlifyA(AbstractPage $page)
    {
        // get label and title for translating
        $label = $this->translate($page->getLabel());
        $title = $this->translate($page->getTitle());
        $escaper = $this->view->plugin('escapeHtml');
        //Get attribs for anchor element
        $attribs = array(
            'id' => $page->getId(),
            'title' => $title,
            'class' => $page->getClass(),
            'href' => $page->getHref(),
            'target' => $page->getTarget()
        );
        if($this->renderIcons) {
            $iconHtml = $this->htmlifyIcon($page);
        } else {
            $iconHtml = '';
        }
        $html = '<a' . $this->htmlAttribs($attribs) . '>' . $iconHtml . $escaper($label) . '</a>';
        return $html;
    }

    protected function htmlifyIcon(AbstractPage $item)
    {
        if($item->icon) {
            $iClass = $item->icon;
            if($this->inverseIcon && $item->isActive(true)) {
                $classes = explode(' ', $iClass);
                $iconWhiteClassKey = array_search('icon-white', $classes);
                if($iconWhiteClassKey === false) {
                    //icon-white class not found
                    $iClass .= ' icon-white';
                } else {
                    //icon-white class found
                    unset($classes[$iconWhiteClassKey]);
                    $iClass = implode(' ', $classes);
                }
            }
            $icon = '<i class="' . $iClass . '"></i> ';
        } else {
            $icon = '';
        }
        return $icon;
    }

    /**
     * View helper entry point:
     * Retrieves helper and optionally sets container to operate on
     * @param  \Zend\Navigation\Navigation $container [optional] container to operate on
     * @return TbNavbar    fluent interface, returns self
     */
    public function __invoke(Navigation $container = null)
    {
        if (null !== $container) {
            $this->setContainer($container);
        }
        return $this;
    }

    protected function translate($text)
    {
        $t = $this->getTranslator();
        if ($this->isTranslatorEnabled()
            && $t
            && is_string($text)
            && !empty($text)) {
                $text = $t->translate($text);
        }
        return $text;
    }

    /**
     * If missing in the text, adds the space separated word to the text
     * @param string $word
     * @param string $text
     */
    protected function addWord($word, &$text)
    {
        $text = trim($text);
        if(!$text) {
            $wordsLower = array();
            $words = array();
        } else {
            $wordsLower = explode(' ', strtolower($text));
            $words = explode(' ', $text);
        }
        if(!in_array(strtolower($word), $wordsLower)) {
            $words[] = $word;
            $text = implode(' ', $words);
        }
    }

    /**
     * Sets CSS class to use for the ul element
     *
     * @param string $class
     * @return \Zf2tb\View\Helper\Navigation\AbstractHelper
     */
    public function setUlClass($class)
    {
        $this->ulClass = (string) $class;
        return $this;
    }

    /**
     * Adds CSS class to the ul element
     *
     * @param string $class
     * @return \Zf2tb\View\Helper\Navigation\AbstractHelper
     */
    public function addUlClass($class)
    {
        $this->ulClass .= ' ' . (string) $class;
        return $this;
    }

    /**
     * Sets CSS class to use for the li element containing submenu
     *
     * @param string $class
     * @return \Zf2tb\View\Helper\Navigation\AbstractHelper
     */
    public function setParentMenuClass($class)
    {
        $this->parentMenuClass = (string) $class;
        return $this;
    }

    /**
     * Adds CSS class to the li element containing submenu
     *
     * @param string $class
     * @return \Zf2tb\View\Helper\Navigation\AbstractHelper
     */
    public function addParentMenuClass($class)
    {
        $this->parentMenuClass .= ' ' . (string) $class;
        return $this;
    }

    /**
     * Sets CSS class to use for the ul element of a submenu
     *
     * @param string $class
     * @return \Zf2tb\View\Helper\Navigation\AbstractHelper
     */
    public function setDropdownUlClass($class)
    {
        $this->dropdownUlClass = (string) $class;
        return $this;
    }

    /**
     * Adds CSS class to the ul element containing a submenu
     *
     * @param string $class
     * @return \Zf2tb\View\Helper\Navigation\AbstractHelper
     */
    public function addDropdownUlClass($class)
    {
        $this->dropdownUlClass .= ' ' . (string) $class;
        return $this;
    }

    /**
     * Sets whether to render icons
     *
     * @param bool $flag
     * @return \Zf2tb\View\Helper\Navigation\AbstractHelper
     */
    public function setRenderIcons($flag = true)
    {
        $this->renderIcons = (bool) $flag;
        return $this;
    }

    /**
     * Sets whether to use caret in dropdown parent menu item
     *
     * @param bool $flag
     * @return \Zf2tb\View\Helper\Navigation\AbstractHelper
     */
    public function setUseDropdownParentCaret($flag = true)
    {
        $this->useDropdownParentCaret = (bool) $flag;
        return $this;
    }
}
