<?php

namespace Zf2tb\View\Helper\Navigation;

use Zend\Navigation\Navigation;
use Zend\Navigation\Page\AbstractPage;

/**
 * TbBreadcrumbs
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class TbBreadcrumbs extends AbstractNavHelper
{
    /**
     * CSS class to use for the ul element
     *
     * @var string
     */
    protected $ulClass = 'breadcrumb';

    protected $divider;

    /**
     * Renders helper
     * @param  string|\Zend\Navigation\AbstractContainer $container [optional] container to render.
     *                                         Default is null, which indicates
     *                                         that the helper should render
     *                                         the container returned by {@link
     *                                         getContainer()}.
     * @return string helper output
     * @throws \Zend\View\Exception\ExceptionInterface if unable to render
     */
    public function render($container = null)
    {
        return $this->renderBreadcrumbs($container);
    }

    public function renderBreadcrumbs(Navigation $container = null, $divider = '/')
    {
        $this->divider = $divider;
        if (null === $container) {
            $container = $this->getContainer();
        }
        $active = $this->findActive($container);
        $activePage = $active['page'];

        $html = $this->renderItem($activePage, false);
        while ($parent = $activePage->getParent()) {
            if ($parent instanceof AbstractPage) {
                $html = $this->renderItem($parent, false) . $html;
            }
            if ($parent === $container) {
                break;
            }
            $activePage = $parent;
        }

        return $this->decorateContainer($html);
    }

    /**
     * Returns an HTML string containing an 'a' element for the given page
     * @param \Zend\Navigation\Page\AbstractPage $page
     * @param bool $renderIcons
     * @param bool $activeIconInverse
     * @return string
     */
    public function htmlifyA(AbstractPage $page)
    {
        // get label and title for translating
        $label = $this->translate($page->getLabel());
        $title = $this->translate($page->getTitle());
        $escaper = $this->view->plugin('escapeHtml');
        //Get attribs for anchor element
        $attribs = array(
            'id' => $page->getId(),
            'title' => $title,
            'class' => $page->getClass(),
            'href' => $page->getHref(),
            'target' => $page->getTarget()
        );

        if ($page->hasPages()) {
            $divider = ' <span class="divider">' . $this->divider . '</span>';
        } else {
            $divider = '';
        }

        if ($page->hasPages()) {
            $html = '<a' . $this->htmlAttribs($attribs) . '>' . $escaper($label) . '</a>' . $divider;
        } else {
            $html = $escaper($label);
        }

        return $html;
    }

    protected function decorateLinkInDropdown($content,
                                              AbstractPage $page,
                                              array $options = array()) {
        //Active
        if(!$page->hasPages()) {
            $liClass = ' class="active"';
        } else {
            $liClass = '';
        }
        $html = '<li' . $liClass . '>' . $content . '</li>';
        return $html;
    }
}
