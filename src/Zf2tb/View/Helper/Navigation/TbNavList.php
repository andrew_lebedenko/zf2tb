<?php

namespace Zf2tb\View\Helper\Navigation;

use Zend\Navigation\Navigation;

/**
 * TbNavList
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class TbNavList extends AbstractNavHelper
{
    /**
     * CSS class to use for the ul element
     *
     * @var string
     */
    protected $ulClass = 'nav nav-list';

    /**
     * Css class to use for the li element containing dropdown submenu
     *
     * @var string
     */
    protected $parentMenuClass = 'dropdown-submenu';

    /**
     * Whether caret should be rendered for dropdown parent item
     *
     * @var boolean
     */
    protected $useDropdownParentCaret = false;

    /**
     * View helper entry point:
     * Retrieves helper and optionally sets container to operate on
     *
     * @param  AbstractContainer $container [optional] container to operate on
     * @return Menu
     */
    public function __invoke(Navigation $container = null)
    {
        if (null !== $container) {
            $this->setContainer($container);
        }

        return $this;
    }

    /**
     * Renders helper
     * @param  string|\Zend\Navigation\AbstractContainer $container [optional] container to render.
     *                                         Default is null, which indicates
     *                                         that the helper should render
     *                                         the container returned by {@link
     *                                         getContainer()}.
     * @return string helper output
     * @throws \Zend\View\Exception\ExceptionInterface if unable to render
     */
    public function render($container = null)
    {
        return $this->renderNavList($container);
    }

    /**
     * Renders NavList
     * @param null|\Zend\Navigation\AbstractContainer $container
     * @param bool $renderIcons
     * @return string
     */
    public function renderNavList(Navigation $container = null)
    {
        if (null === $container) {
            $container = $this->getContainer();
        }

        $html = '';

        $options = array(
            'ulClass' => $this->ulClass,
        );

        $html .= PHP_EOL . $this->renderContainer($container, $options);

        return $html;
    }
}
