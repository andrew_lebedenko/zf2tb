<?php

namespace Zf2tb\View\Helper\Navigation;

use Zend\Navigation\Navigation;

/**
 * TbTabs
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class TbTabs extends AbstractNavHelper
{
    const TYPE_TABS = 'nav-tabs';

    const TYPE_PILLS = 'nav-pills';

    const TYPE_STACKED = 'nav-stacked';

    protected $navTypes = [
        self::TYPE_TABS,
        self::TYPE_PILLS,
    ];

    protected $navType = self::TYPE_TABS;

    protected $stacked = false;
    /**
     * Renders helper
     *
     * @param  string|\Zend\Navigation\AbstractContainer $container [optional] container to render.
     *                                         Default is null, which indicates
     *                                         that the helper should render
     *                                         the container returned by {@link
     *                                         getContainer()}.
     * @return string helper output
     * @throws \Zend\View\Exception\ExceptionInterface if unable to render
     */
    public function render($container = null)
    {
        return $this->renderTabs($container);
    }

    /**
     * Renders Tabs
     * @param null|\Zend\Navigation\Navigation $container
     * @param bool $pills
     * @param bool $stacked
     * @param bool $renderIcons
     * @return string
     */
    public function renderTabs(Navigation $container = null)
    {
        if (null === $container) {
            $container = $this->getContainer();
        }
        //Tabs or Pills
        $this->ulClass = 'nav ' . $this->navType;
        if($this->navType == self::TYPE_PILLS) {
            $this->inverseIcon = true;
        } else {
            $this->inverseIcon = false;
        }
        //Stacked
        if($this->stacked) {
            $this->ulClass .= ' ' . self::TYPE_STACKED;
        }

        $html = PHP_EOL . $this->renderContainer($container);

        return $html;
    }

    public function setNavType($type)
    {
        if (in_array($type, $this->navTypes)) {
            $this->navType = $type;
        }
        return $this;
    }

    public function setStacked($stacked = true)
    {
        $this->stacked = (bool) $stacked;
        return $this;
    }
}
