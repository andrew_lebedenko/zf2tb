<?php

namespace Zf2tb\View\Helper\Navigation\Exception;

use Zf2tb\Exception\ExceptionInterface as Zf2tbExceptionInterface;

/**
 * Exception Interface
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
interface ExceptionInterface extends Zf2tbExceptionInterface
{
}
