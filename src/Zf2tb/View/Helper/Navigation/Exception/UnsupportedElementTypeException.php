<?php

namespace Zf2tb\View\Helper\Navigation\Exception;

use Zf2tb\View\Helper\Navigation\Exception\ExceptionInterface;

/**
 * UnsupportedElementTypeException
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class UnsupportedElementTypeException extends \Exception implements ExceptionInterface
{
}
