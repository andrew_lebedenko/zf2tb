<?php

namespace Zf2tb\View\Helper\Navigation;

use Zend\ServiceManager\ConfigInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * PluginConfigurator
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
class PluginConfigurator implements ConfigInterface
{
    /**
     * @var array Nav View helpers
     */
    protected $helpers = array(
        'tbNavbar' => 'Zf2tb\View\Helper\Navigation\TbNavbar',
        'tbNavList' => 'Zf2tb\View\Helper\Navigation\TbNavList',
        'tbTabs' => 'Zf2tb\View\Helper\Navigation\TbTabs',
        'tbButtons' => 'Zf2tb\View\Helper\Navigation\TbButtons',
        'tbBreadcrumbs' => 'Zf2tb\View\Helper\Navigation\TbBreadcrumbs',
    );

    public function configureServiceManager(ServiceManager $serviceManager)
    {
        foreach($this->helpers as $name => $fqcn) {
            $serviceManager->setInvokableClass($name, $fqcn);
        }
    }
}
