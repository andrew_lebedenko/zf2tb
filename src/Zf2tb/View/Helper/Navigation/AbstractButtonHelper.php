<?php

namespace Zf2tb\View\Helper\Navigation;

use Zend\Navigation\Page\AbstractPage;

/**
 * Abstract Button Helper
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */
abstract class AbstractButtonHelper extends AbstractHelper
{
    const TYPE_SINGLE_HORIZONTAL = 'singleHorizontal';

    const TYPE_SINGLE_VERTICAL = 'singleVertical';

    const TYPE_GROUPS_HORIZONTAL = 'groupsHorizontal';

    const TYPE_GROUPS_VERTICAL = 'groupsVertical';

    const TYPE_ONE_GROUP = 'oneGroup';

    protected $buttonGroupOpen = false;

    protected function decorateContainer($content, array $options = array())
    {
        if(array_key_exists('type', $options) && (
           $options['type'] == self::TYPE_GROUPS_HORIZONTAL
           || $options['type'] == self::TYPE_GROUPS_VERTICAL
           || $options['type'] == self::TYPE_ONE_GROUP)) {
            $content    .= $this->closeButtonGroup();
        }
        $html = '<div class="btn-toolbar">';
        $html .= PHP_EOL . $content;
        $html .= PHP_EOL . '</div>';

        return $html;
    }

    protected function decorateNavHeader($content, AbstractPage $item, array $options = array())
    {
        return '';
    }

    protected function decorateNavHeaderInDropdown($content, AbstractPage $item, array $options = array())
    {
        return '';
    }

    protected function decorateDivider($content, AbstractPage $item, array $options = array())
    {
        if(array_key_exists('type', $options) && $options['type'] == self::TYPE_GROUPS_HORIZONTAL) {
            //Groups horizontal
            $html   = $this->closeButtonGroup() . $content;
        } elseif(array_key_exists('type', $options) && $options['type'] == self::TYPE_GROUPS_VERTICAL) {
            //Groups vertical
            $html   = $this->closeButtonGroup() . $content;
            $html   .= PHP_EOL . '<br>';
        } else {
            //Non grouped - do not render divider
            $html   = '';
        }
        return $html;
    }

    protected function decorateLink($content, AbstractPage $page, array $options = array())
    {
        if(array_key_exists('type', $options) && $options['type'] == self::TYPE_SINGLE_HORIZONTAL) {
            $html = '<div class="btn-group">'
                  . PHP_EOL . $content
                  . '</div>';
        } elseif(array_key_exists('type', $options) && $options['type'] == self::TYPE_SINGLE_VERTICAL) {
            $html = '<div class="btn-group">'
                  . PHP_EOL . $content
                  . '</div><br>';
        } else {
            //One of the grouped types
            $html = $this->openButtonGroup() . $content;
        }
        return $html;
    }

    protected function decorateDropdown($content, AbstractPage $page, array $options = array())
    {
        $html = $this->closeButtonGroup();
        $html .= '<div class="btn-group">'
            . PHP_EOL . $content
            . PHP_EOL . '</div>';
        if(array_key_exists('type', $options) && (
            $options['type'] == self::TYPE_SINGLE_VERTICAL
            || $options['type'] == self::TYPE_GROUPS_VERTICAL
        )) {
            $html .= PHP_EOL . '<br>';
        }
        return $html;
    }

    protected function renderLink(AbstractPage $page, array $options = array())
    {
        $class = $page->getClass();
        $this->addWord('btn', $class);
        if($page->isActive(true)) {
            $this->addWord('active', $class);
        }
        $page->setClass($class);
        $html = parent::renderLink($page, $options);
        return $html;
    }

    protected function renderLinkInDropdown(AbstractPage $page, array $options = array())
    {
        $html = parent::renderLink($page, $options);
        return $html;
    }

    protected function renderDropdown(AbstractPage $page, array $options = array())
    {
        $class = $page->getClass();
        $this->addWord('btn', $class);
        if($page->isActive(true)) {
            $this->addWord('active', $class);
        }
        $page->setClass($class);
        $html = parent::renderDropdown($page, $options);
        return $html;
    }

    protected function openButtonGroup()
    {
        if(!$this->buttonGroupOpen) {
            $this->buttonGroupOpen = true;
            $html = PHP_EOL . '<div class="btn-group">';
        } else {
            $html = '';
        }
        return $html;
    }

    protected function closeButtonGroup()
    {
        if($this->buttonGroupOpen) {
            $this->buttonGroupOpen = false;
            $html = PHP_EOL . '</div>';
        } else {
            $html = '';
        }
        return $html;
    }
}
