<?php
/* ************************* NOTE ******************************
 * If you do not have your own layout view script referencing  *
 * the necessary Twitter Bootstrap and jQuery dependencies     *
 * move this file to <your project>/config/autoload directory  *
 * *************************************************************
 */

/**
 * Zf2tb - Global configuration override
 * Responsibility: Set layout to the layout script provided with the Zf2tb package to set-up
 * the Twitter Bootstrap environment.
 *
 * @package Zf2tb
 * @author Andrew Lebedenko
 * @copyright Andrew Lebedenko (c)
 * @link https://bitbucket.org/andrew_lebedenko/zf2tb
 */

return array(
    'view_manager' => array(
        'layout' => 'layout/layoutzf2tb',
    ),
);
