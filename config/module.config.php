<?php

return array(
    'view_manager' => array(
        'template_map' => array(
            'layout/layoutzf2tb' => __DIR__ . '/../view/layout/layoutzf2tb.phtml',
        ),
        'template_path_stack' => array(
            'zf2tb' => __DIR__ . '/../view',
        ),
    ),
    'service_manager' => array(
        'invokables' => array(
            'zf2tb_gen_util' => 'Zf2tb\GenUtil',
            'zf2tb_form_util' => 'Zf2tb\Form\FormUtil',
            'zf2tb_nav_view_helper_configurator' => 'Zf2tb\View\Helper\Navigation\PluginConfigurator',
         ),
    ),
    'Zf2tb' => array(
        'sup_ver_zf2' => '2.2.0',
        'sup_ver_zf2tb' => '2.1.0',
    ),
);
